<?php namespace App\Controllers;

use App\Models\guestModel;
use App\Models\reservationModel;
use App\Models\RoomModel;
use Aws\S3\S3Client;

class reservation extends BaseController


{
    public function index() //Обображение всех записей
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if ($this->ionAuth->isAdmin()) {

            //Подготовка значения количества элементов выводимых на одной странице
            if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
            {
                //сохранение кол-ва страниц в переменной сессии
                session()->setFlashdata('per_page', $this->request->getPost('per_page'));
                $per_page = $this->request->getPost('per_page');
            } else {
                $per_page = session()->getFlashdata('per_page');
                session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
                if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
            }
            $data['per_page'] = $per_page;
            //Обработка запроса на поиск
            if (!is_null($this->request->getPost('search'))) {
                session()->setFlashdata('search', $this->request->getPost('search'));
                $search = $this->request->getPost('search');
            } else {
                $search = session()->getFlashdata('search');
                session()->setFlashdata('search', $search);
                if (is_null($search)) $search = '';
            }
            $data['search'] = $search;
            helper(['form', 'url']);
            $model = new reservationModel();
            $data['reservation'] = $model->getReservationsWithGuests(null, $search)->paginate($per_page, 'group1');
            $data['pager'] = $model->pager;
            //   var_dump($data);
            //    die();
            echo view('reservation/view_all', $this->withIon($data));
        } else {
            session()->setFlashdata('message', lang('Curating.admin_permission_needed'));
            return redirect()->to('/auth/login');
        }
    }


    public function view($id) //отображение одной записи
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new reservationModel();
        $data ['reservation'] = $model->getReservationsWithGuests($id);
        //var_dump($data);
        //  die();
        echo view('reservation/view', $this->withIon($data));
    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        $model = new guestModel();
        $data ['guests'] = $model->getGuest();
        $model = new RoomModel();
        $data ['room'] = $model->getRoom();
        //
        echo view('reservation/create', $this->withIon($data));
    }

    public function store()
    {
        helper(['form', 'url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'ID_guest' => 'required|greater_than[0]',
                'ID_room' => 'required|greater_than[0]',
                'date_beginning' => 'required',
                'date_end' => 'required',
                'number_peoples' => 'required|greater_than[0]',
                'picture' => 'is_image[picture]|max_size[picture,1024]',
            ])) {
            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);

            }

            $model = new reservationModel();

            $data = [
                'ID_guest' => $this->request->getPost('ID_guest'),
                'ID_room' => $this->request->getPost('ID_room'),
                'date_beginning' => $this->request->getPost('date_beginning'),
                'date_end' => $this->request->getPost('date_end'),
                'number_peoples' => $this->request->getPost('number_peoples'),
            ];
            //если изображение было загружено и была получена ссылка на него то даобавить ссылку в данные модели
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];

            $model->save($data);
            session()->setFlashdata('message', lang('Бронирование успешно создано'));
            return redirect()->to('/reservation');
        } else {
            return redirect()->to('/reservation/create')->withInput();
        }
    }

    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }

        helper(['form']);
        $model = new reservationModel();
        $data ['reservation'] = $model->getreservation($id);
        //var_dump($data ['reservation']); die();
        $data ['validation'] = \Config\Services::validation();
        $model = new guestModel();
        $data ['guests'] = $model->getGuest();
        $model = new RoomModel();
        $data ['room'] = $model->getRoom();
        echo view('reservation/edit', $this->withIon($data));

    }

    public function update()
    {
        helper(['form', 'url']);
        echo '/reservation/edit/' . $this->request->getPost('id');
        if
        ($this->request->getMethod() === 'post' && $this->validate([
                'id' => 'required',
                'ID_guest' => 'required|greater_than[0]',
                'date_beginning' => 'required',
                'date_end' => 'required',
                'ID_room' => 'required|greater_than[0]',
            ])) {

            $model = new reservationModel();
            $model->save([
                'ID' => $this->request->getPost('id'),
                'ID_guest' => $this->request->getPost('ID_guest'),
                'data_beginning' => $this->request->getPost('data_beginning'),
                'data_end' => $this->request->getPost('data_end'),
                'ID_room' => $this->request->getPost('ID_room'),

            ]);
            session()->setFlashdata('message', lang('Бронирование успешно обновлено'));

            return redirect()->to('/reservation');
        } else {
            return redirect()->to('/reservation/edit/' . $this->request->getPost('id'))->withInput();
        }
    }

    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn())
            return redirect()->to('/auth/login');
        else {
            $model = new reservationModel();
            $model->delete($id);
            //return redirect()->to('/reservation');
            session()->setFlashdata('message', lang('Бронирование успешно удалено'));
            return redirect()->to('/reservation');
        }
    }
}

//    public function viewAllWithUsers()
//    {
//        if ($this->ionAuth->isAdmin())
//        {
//            $model = new reservationModel();
//            $data['reservation'] = $model->getReservationsWithGuests()->paginate(2, 'group1');
//            $data['pager'] = $model->pager;
//            echo view('reservation/view_all', $this->withIon($data));
//        }
//        else
//        {
//            session()->setFlashdata('message', lang('reservation.admin_permission_needed'));
//            return redirect()->to('/auth/login');
//        }
//    }}
