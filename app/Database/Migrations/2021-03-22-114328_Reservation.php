<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Reservation extends Migration
{
	public function up()
	{
        // activity_type
        if (!$this->db->tableexists('Buildings'))
        {
            // Setup Keys
            $this->forge->addkey('ID', TRUE);

            $this->forge->addfield(array(
                'ID' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE),
            ));
            $this->forge->createtable('Buildings', TRUE);
        }

        if (!$this->db->tableexists('guest'))
        {
            // Setup Keys
            $this->forge->addkey('ID', TRUE);

            $this->forge->addfield(array(
                'ID' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
            ));
            // create table
            $this->forge->createtable('guest', TRUE);
        }

        // activity_type
        if (!$this->db->tableexists('room'))
        {
            // Setup Keys
            $this->forge->addkey('ID', TRUE);

            $this->forge->addfield(array(
                'ID' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'ID_Buildings' => array('type' => 'INT', 'unsigned' => TRUE),
                'number' => array('type' => 'INT', 'null' => FALSE),
                'places' => array('type' => 'INT', 'null' => FALSE),
                'price' => array('type' => 'INT', 'null' => FALSE),

            ));
            $this->forge->addForeignKey('ID_Buildings','Buildings','ID','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('room', TRUE);
        }

        // activity_type
        if (!$this->db->tableexists('reservation'))
        {
            // Setup Keys
            $this->forge->addkey('ID', TRUE);

            $this->forge->addfield(array(
                'ID' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'ID_room' => array('type' => 'INT', 'unsigned' => TRUE),
                'ID_guest' => array('type' => 'INT', 'unsigned' => TRUE),
                'date_beginning' => array('type' => 'DATE', 'null' => FALSE),
                'date_end' => array('type' => 'DATE', 'null' => FALSE),
                'number_peoples' => array('type' => 'int', 'null' => FALSE),
            ));
            $this->forge->addForeignKey('ID_room','room','ID','RESTRICT','RESRICT');
            $this->forge->addForeignKey('ID_guest','guest','ID','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('reservation', TRUE);
        }


        // activity_type
        if (!$this->db->tableexists('residence'))
        {
            // Setup Keys
            $this->forge->addkey('ID', TRUE);

            $this->forge->addfield(array(
                'ID' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'ID_room' => array('type' => 'INT', 'unsigned' => TRUE),
                'ID_guest' => array('type' => 'INT', 'unsigned' => TRUE),
                'date_beginning' => array('type' => 'DATE', 'null' => FALSE),
                'date_end' => array('type' => 'DATE', 'null' => FALSE),
                'number_peoples' => array('type' => 'int', 'null' => FALSE),
            ));
            $this->forge->addForeignKey('ID_room','room','ID','RESTRICT','RESRICT');
            $this->forge->addForeignKey('ID_guest','guest','ID','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('residence', TRUE);
        }

	}

	public function down()
	{
        $this->forge->droptable('residence');
        $this->forge->droptable('reservation');
        $this->forge->droptable('room');
        $this->forge->droptable('guest');
        $this->forge->droptable('Buildings');
	}
}
