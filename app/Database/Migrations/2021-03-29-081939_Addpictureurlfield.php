<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Addpictureurlfield extends Migration
{
	public function up()
	{
        if ($this->db->tableexists('reservation'))
        {
            $this->forge->addColumn('reservation',array(
                'picture_url' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE)
            ));
        }
	}

	public function down()
	{
        $this->forge->dropColumn('reservation', 'picture_url');
	}
}
