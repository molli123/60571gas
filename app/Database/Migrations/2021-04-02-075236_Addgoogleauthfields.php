<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Addgoogleauthfields extends Migration
{
	public function up()
	{
        if ($this->db->tableexists('users'))
        {
            $this->forge->addColumn('users',array(
                'google_id' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE),
            ));
            $this->forge->addColumn('users',array(
                'locale' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE)
            ));
            $this->forge->addColumn('users',array(
                'picture_url' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE)
            ));
        }
    }

	public function down()
	{
        $this->forge->dropColumn('users', 'google_id');
        $this->forge->dropColumn('users', 'locale');
        $this->forge->dropColumn('users', 'picture_url');
    }
}
