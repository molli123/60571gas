<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class reservation extends Seeder
{
    public function run()
    {
        $data = [

            'name' => 'Главный корпус',
        ];
        $this->db->table('Buildings')->insert($data);

        $data = [

            'name' => 'Маленький корпус',
        ];
        $this->db->table('Buildings')->insert($data);

        $data = [

            'name'=> 'Петров Павел Владимирович',
        ];
        $this->db->table('guest')->insert($data);

        $data = [

            'name'=> 'Доронин Александра Дмитриевна',
        ];
        $this->db->table('guest')->insert($data);

        $data = [

            'name'=> 'Царь Дмитрий Павлович',
        ];
        $this->db->table('guest')->insert($data);

        $data = [

            'name'=> 'Найденкова Мария Игоревна',
        ];
        $this->db->table('guest')->insert($data);

        $data = [

            'name'=> 'Кузьмин Максим Игоревич',
        ];
        $this->db->table('guest')->insert($data);

        $data = [

            'name'=> 'Куприна Елена Анатольевна',
        ];
        $this->db->table('guest')->insert($data);

        $data = [

            'name'=> 'Купрова Ксения Максимовна',
        ];
        $this->db->table('guest')->insert($data);

        $data = [

            'ID_Buildings'=> '1',
            'number'=>'1',
            'places' => '3',
            'price' => '10000',
        ];
        $this->db->table('room')->insert($data);

        $data = [

            'ID_Buildings'=> '1',
            'number'=>'2',
            'places' => '2',
            'price' => '7500',
        ];
        $this->db->table('room')->insert($data);

        $data = [

            'ID_Buildings'=> '2',
            'number'=>'3',
            'places' => '2',
            'price' => '5000',
        ];
        $this->db->table('room')->insert($data);

        $data = [

            'ID_Buildings'=> '2',
            'number'=>'4',
            'places' => '1',
            'price' => '3500',
        ];
        $this->db->table('room')->insert($data);

        $data = [

            'ID_Buildings'=> '1',
            'number'=>'5',
            'places' => '4',
            'price' => '12000',
        ];
        $this->db->table('room')->insert($data);

        $data = [

            'ID_room'=> '1',
            'ID_guest'=>'1',
            'date_beginning' => '2021-03-20',
            'date_end' => '2021-03-25',
            'number_peoples' => '3',
        ];
        $this->db->table('reservation')->insert($data);

        $data = [

            'ID_room'=> '2',
            'ID_guest'=>'2',
            'date_beginning' => '2021-03-22',
            'date_end' => '2021-03-30',
            'number_peoples' => '2',
        ];
        $this->db->table('reservation')->insert($data);

        $data = [

            'ID_room'=> '3',
            'ID_guest'=>'3',
            'date_beginning' => '2021-03-20',
            'date_end' => '2021-03-28',
            'number_peoples' => '2',
        ];
        $this->db->table('reservation')->insert($data);

        $data = [

            'ID_room'=> '4',
            'ID_guest'=>'4',
            'date_beginning' => '2021-03-18',
            'date_end' => '2021-04-01',
            'number_peoples' => '1',
        ];
        $this->db->table('reservation')->insert($data);

        $data = [

            'ID_room'=> '5',
            'ID_guest'=>'5',
            'date_beginning' => '2021-04-01',
            'date_end' => '2021-04-10',
            'number_peoples' => '4',
        ];
        $this->db->table('reservation')->insert($data);

        $data = [

            'ID_room'=> '5',
            'ID_guest'=>'6',
            'date_beginning' => '2021-05-20',
            'date_end' => '2021-05-30',
            'number_peoples' => '4',
        ];
        $this->db->table('residence')->insert($data);

        $data = [

            'ID_room'=> '2',
            'ID_guest'=>'7',
            'date_beginning' => '2021-01-15',
            'date_end' => '2021-01-20',
            'number_peoples' => '2',
        ];
        $this->db->table('residence')->insert($data);

        $data = [

            'ID_room'=> '3',
            'ID_guest'=>'1',
            'date_beginning' => '2021-02-01',
            'date_end' => '2021-02-08',
            'number_peoples' => '2',
        ];
        $this->db->table('residence')->insert($data);

        $data = [

            'ID_room'=> '4',
            'ID_guest'=>'2',
            'date_beginning' => '2021-01-20',
            'date_end' => '2021-01-26',
            'number_peoples' => '1',
        ];
        $this->db->table('residence')->insert($data);

    }
}

