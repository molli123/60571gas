<?php


namespace App\Models;


class RoomModel extends \CodeIgniter\Model
{
    protected $table = 'room'; //таблица, связанная с моделью
    protected $allowedFields = ['ID', 'number'];
    public function getRoom($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
}