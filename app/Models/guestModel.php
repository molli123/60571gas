<?php


namespace App\Models;


class guestModel extends \CodeIgniter\Model
{
    protected $table = 'guest'; //таблица, связанная с моделью
    protected $allowedFields = ['name', 'ID'];
    public function getGuest($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
}