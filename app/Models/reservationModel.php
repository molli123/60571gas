<?php namespace App\Models;

use CodeIgniter\Model;
class reservationModel extends Model
{
    protected $table = 'reservation'; //таблица, связанная с моделью
    protected $allowedFields = ['ID', 'ID_guest','ID_room','date_beginning', 'date_end', 'number_peoples', 'picture_url'];
    protected $primaryKey = 'ID';

    public function getreservation($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['ID' => $id])->first();
    }
    public function getReservationsWithGuests($id = null, $search = '')
    {
//        $builder = $this->db->table('reservation');
//        $builder->select( '*, reservation.ID as ID');
//        $builder->join('guest', 'reservation.ID_guest = guest.ID');
//        if (!is_null($id))
//        {
//            $rows = $builder->getWhere(['reservation.ID' => $id])->getResult('array');
//            return $rows[0];
//        }
//        return $builder->get()->getResult('array');
// закоментила Ира
        $builder = $this->select('*, reservation.ID')
            ->join('guest', 'reservation.ID_guest = guest.ID', 'LEFT')
            ->like('name', $search,'both', null, true);
        if (!is_null($id))
        {
            return $builder->where(['reservation.ID' => $id])->first();
        }
        return $builder;
//конец комента
    }
//    public function getReservationsWithRoom($id = null, $search = '')
//    {
//
//        $builder = $this->select('*')->join('room', 'reservation.ID_room = room.ID')->like('name', $search,'both', null, true)->orlike('number', $search,'both', null, true);
//        if (!is_null($id))
//        {
//            return $builder->where(['reservation.ID' => $id])->first();
//        }
//        return $builder;
//    }

}