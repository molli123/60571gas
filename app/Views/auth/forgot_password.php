<h1><?php echo lang('Восстановление пароля');?></h1>
<p><?php echo sprintf(lang('Пожалуйста введите свой email, куда мы отправим вам информацию для восстановления пароля'), $identity_label);?></p>

<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open('auth/forgot_password');?>

      <p>
      	<label for="identity"><?php echo (($type === 'email') ? sprintf(lang('Auth.forgot_password_email_label'), $identity_label) : sprintf(lang('Auth.forgot_password_identity_label'), $identity_label));?></label> <br />
      	<?php echo form_input($identity);?>
      </p>

      <p><?php echo form_submit('submit', lang('Отправить'));?></p>

<?php echo form_close();?>
