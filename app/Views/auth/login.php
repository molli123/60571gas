<h1><?php echo lang('Auth.login_heading'); ?></h1>
<p><?php echo lang('Auth.login_subheading'); ?></p>
<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container" style="padding: 150px 0 116px;">
    <div class="row justify-content-center">
        <div class="col-md-8 col-12">
            <div style="background: rgba(0,0,0,0.5); padding: 30px;">
                <h1><?php echo lang('Вход'); ?></h1>
                <p><?php echo lang('Для входа используйте email/имя пользователя и пароль.'); ?></p>

                <?php if (isset($message)): ?>
                    <div class="alert alert-danger"><?php echo $message; ?></div>
                <?php endif ?>

                <?php echo form_open('auth/login'); ?>
                <div class="mb-3">
                    <?php echo form_label(lang('Email:'), 'identity'); ?>
                    <?php echo form_input($identity, '', 'class="form-control" value="Mark" required'); ?>
                </div>
                <div class="mb-3">
                    <?php echo form_label(lang('Пароль:'), 'password'); ?>
                    <?php echo form_input($password, '', 'class="form-control" value="Mark" required'); ?>
                </div>
                <div class="mb-3">
                    <?php echo form_label(lang('Запомнить меня:'), 'remember'); ?>
                    <?php echo form_checkbox('remember', '1', false, 'id="remember"'); ?>
                </div>
                <div class="mb-3 row justify-content-center">
                    <?php echo form_submit('submit', lang('Вход'), 'class="btn btn-dark"'); ?>
                </div>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<div style="background: rgba(0,0,0,0.5); padding: 30px;">
    <p class="text-center"><a href="forgot_password"><?php echo lang('Забыли пароль?'); ?></a>&nbsp<a
                href="register_user"><?php echo lang('Создать учетную запись'); ?></a></p>
    <div class="mb-3 row justify-content-center">
        <a href="<?= $authUrl; ?>" class="btn btn-outline-light" role="button" style="text-transform:none">
            <img width="20px" style="margin-bottom:3px; margin-right:5px" alt="Google sign-in"
                 src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/512px-Google_%22G%22_Logo.svg.png"/>
            <?= lang('Войти с Google') ?>
        </a>
    </div>
</div>

<div id="infoMessage"><?php echo $message; ?></div>
<?= $this->endSection() ?>