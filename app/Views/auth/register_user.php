<?= $this->extend('templates/layout');
echo strlen($message) ?>
<?= $this->section('content') ?>

    <div class="container" style="padding: 150px 0 116px;">
        <div class="row justify-content-center">
            <div class="col-md-6 col-12 ">
                <div style="background: rgba(0,0,0,0.5); padding: 30px;">

                    <h1><?php echo lang('Вход'); ?></h1>
                    <p><?php echo lang('Для входа используйте email/имя пользователя и пароль'); ?></p>

                    <?php echo form_open('auth/register_user'); ?>
                    <div class="mb-3">
                        <?php echo form_label(lang('Имя:'), 'first_name'); ?> <br/>
                        <?php echo form_input($first_name, '', 'class="form-control" required'); ?>
                    </div>
                    <div class="mb-3">
                        <?php echo form_label(lang('Фамилия:'), 'last_name'); ?> <br/>
                        <?php echo form_input($last_name, '', 'class="form-control" required'); ?>
                    </div>
                    <?php
                    if ($identity_column !== 'email') {
                        echo '<div class="mb-3">';
                        echo form_label(lang('Auth.create_user_identity_label'), 'identity');
                        echo '<br/>';
                        echo \Config\Services::validation()->getError('identity');
                        echo form_input($identity);
                        echo '</div>';
                    }
                    ?>
                    <div class="mb-3">
                        <?php echo form_label(lang('Компания:'), 'company'); ?> <br/>
                        <?php echo form_input($company, '', 'class="form-control"'); ?>
                    </div>
                    <div class="mb-3">
                        <?php echo form_label(lang('Email:'), 'email'); ?> <br/>
                        <?php echo form_input($email, '', 'class="form-control" required'); ?>
                    </div>
                    <div class="mb-3">
                        <?php echo form_label(lang('Телефон:'), 'phone'); ?> <br/>
                        <?php echo form_input($phone, '', 'class="form-control" required'); ?>
                    </div>
                    <div class="mb-3">
                        <?php echo form_label(lang('Пароль:'), 'password'); ?> <br/>
                        <?php echo form_input($password, '', 'class="form-control" required'); ?>
                    </div>
                    <div class="mb-3">
                        <?php echo form_label(lang('Подтверждение пароля:'), 'password_confirm'); ?> <br/>
                        <?php echo form_input($password_confirm, '', 'class="form-control" required'); ?>
                    </div>
                    <div class="mb-3">
                        <?php echo form_submit('submit', lang('Зарегестрироваться'),'class="btn btn-dark d-block mx-auto"'); ?>
                    </div>
                    <?php echo form_close(); ?>

                </div>
            </div>
        </div>
    </div>
<?= $this->endSection() ?>