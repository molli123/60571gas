<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="jumbotron text-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-8">
                <h1 class="mb-4">Гостиничный комплекс <span>GRAND HOTEL</span></h1>
                <p class="mb-4">Это приложение поможет бронировать номера в гостинице и вести учет по занятости номеров и текущих бронях.</p>
                <a class="btn btn-dark btn-lg" href="auth/login" role="button">Войти</a>
            </div>
        </div>
    </div>

</div>
<?= $this->endSection() ?>
