<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container" style="max-width: 540px; padding: 160px 0 57px 0; ">
    <div style="padding: 30px; background: rgba(0,0,0,0.8)">

        <?= form_open_multipart('reservation/store'); ?>
        <div class="form-group">
            <label for="ID_guest">ФИО</label>
            <select class="form-control <?= ($validation->hasError('ID_guest')) ? 'is-invalid' : ''; ?>" name='ID_guest'
                    onChange="">
                <option value="-1">Выберите гостя</option>
                <?php foreach ($guests as $item): ?>
                    <option value=<?= esc($item['ID']); ?>><?= esc($item['name']); ?> </option>
                <?php endforeach; ?>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('ID_guest') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="ID_room">Номер комнаты</label>
            <select class="form-control  <?= ($validation->hasError('ID_room')) ? 'is-invalid' : ''; ?>" name='ID_room'
                    onChange="">
                <option value="-1">Выберите номер комнаты</option>
                <?php foreach ($room as $item): ?>
                    <option value=<?= esc($item['ID']); ?>><?= esc($item['number']); ?> </option>
                <?php endforeach; ?>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('ID_room') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="birthday">Дата начала</label>
            <input type="date"
                   class="form-control <?= ($validation->hasError('date_beginning')) ? 'is-invalid' : ''; ?>"
                   name="date_beginning" value="<?= old('date_beginning'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('date_beginning') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="birthday">Дата окончания</label>
            <input type="date" class="form-control <?= ($validation->hasError('date_end')) ? 'is-invalid' : ''; ?>"
                   name="date_end" value="<?= old('date_end'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('date_end') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="number_peoples">Количество человек</label>
            <select class="form-control  <?= ($validation->hasError('number_peoples')) ? 'is-invalid' : ''; ?>"
                    name='number_peoples' onChange="">
                <option value="-1">Укажите количество человек</option>

                <option value="1" <?php if (old("number_peoples") == 1) echo("selected") ?>> 1</option>
                <option value="2" <?php if (old("number_peoples") == 2) echo("selected") ?>> 2</option>
                <option value="3" <?php if (old("number_peoples") == 3) echo("selected") ?>> 3</option>
                <option value="4" <?php if (old("number_peoples") == 4) echo("selected") ?>> 4</option>

            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('number_peoples') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="birthday">Изображение</label>
            <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>"
                   name="picture">
            <div class="invalid-feedback">
                <?= $validation->getError('picture') ?>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-dark" name="submit">Создать</button>
        </div>
        </form>

    </div>
</div>
<?= $this->endSection() ?>
