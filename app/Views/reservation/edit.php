<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div class="container" style="max-width: 540px; padding: 200px 0;">
    <div style="background: rgba(0,0,0,.8); padding: 30px;">
        <?= form_open_multipart('reservation/update'); ?>
        <input type="hidden" name="id" value="<?= $reservation["ID"] ?>">
        <div class="form-group">
            <label for="ID_guest">ФИО</label>
            <select class="form-control <?= ($validation->hasError('ID_guest')) ? 'is-invalid' : ''; ?>" name='ID_guest'
                    onChange="">
                <option value="-1">Выберите гостя</option>
                <?php foreach ($guests as $item): ?>
                    <option value=" <?= ($reservation['ID_guest']) ?>" <?php if ($reservation["ID_guest"] == $item['ID']) echo "selected"; ?> >     <?= esc($item['name']); ?> </option>

                <?php endforeach; ?>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('ID_guest') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="number_peoples">Номер комнаты</label>
            <select class="form-control  <?= ($validation->hasError('number_peoples')) ? 'is-invalid' : ''; ?>"
                    name='number_peoples' onChange="">
                <option value="-1">Выберите номер комнаты</option>
                <option value="1" <?php if (old("number_peoples") == 1) echo("selected") ?>> 1</option>
                <option value="2" <?php if (old("number_peoples") == 2) echo("selected") ?>> 2</option>
                <option value="3" <?php if (old("number_peoples") == 3) echo("selected") ?>> 3</option>
                <option value="4" <?php if (old("number_peoples") == 4) echo("selected") ?>> 4</option>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('number_peoples') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="birthday">Дата начала</label>
            <input type="date"
                   class="form-control <?= ($validation->hasError('date_beginning')) ? 'is-invalid' : ''; ?>"
                   name="date_beginning" value="<?= $reservation["date_beginning"] ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('date_beginning') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="birthday">Дата окончания</label>
            <input type="date" class="form-control <?= ($validation->hasError('date_end')) ? 'is-invalid' : ''; ?>"
                   name="date_end" value="<?= $reservation["date_end"] ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('date_end') ?>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-dark" name="submit">Сохранить</button>
        </div>
        </form>
    </div>
</div>
<?= $this->endSection() ?>
