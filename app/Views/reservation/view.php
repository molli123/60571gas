<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="padding: 220px 0;">
        <h2  style="color: #2C2B29; margin-bottom: 35px;">Бронирование</h2>
        <?php if (!empty($reservation) && is_array($reservation)) : ?>
            <?php //foreach ($reservation as $item): ?>
                <div class="card mb-3" style="padding:15px 30px; background: rgba(0,0,0,0.8); ">
                    <div class="row">
                        <div class="col-md-4 d-flex align-items-center">
                            <?php if (is_null($reservation['picture_url'])) : ?>
                                <img src="<?= base_url() ?>/images/avatar.png" class="card-img" alt="<?= esc($reservation['ID']); ?>">
                            <?php else:?>
                                <img src="<?= esc($reservation['picture_url']); ?>" class="card-img" alt="<?= esc($reservation['name']); ?>">
                            <?php endif ?>
                        </div>
                    <div class="col-md-8">
                        <div class="card-body" style="color: white;">
                            <h5 class="card-title"><?= esc($reservation['name']); ?></h5>
                            <div class="d-flex justify-content-between">
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Дата начала бронирования: </div>
                                <?= esc($reservation['date_beginning']); ?>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Дата окончания бронирования: </div>
                                <?= esc($reservation['date_end']); ?>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Номер бронирования: </div>
                                <?= esc($reservation['ID']); ?>
                            </div>
                            <div class="d-flex justify-content-between mb-3">
                                <div class="my-0">Номер комнаты: </div>
                                <?= esc($reservation['ID_room']); ?>
                            </div>
                            <a href="<?= base_url()?>/reservation/edit/<?= esc($reservation['ID']); ?>" class="btn btn-dark">Редактировать</a>
                            <a class="btn btn-dark" href="<?= base_url()?>/reservation/delete/<?= esc($reservation['ID']); ?>">Удалить</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php //endforeach; ?>
        <?php else : ?>
        <p>Невозможно найти бронь.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>