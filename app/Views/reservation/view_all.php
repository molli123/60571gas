<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main" style="padding: 150px;">
        <h2 style="color: #2C2B29;" class="mb-3">Все бронирования</h2>
        <div class="d-flex mb-3">
            <?= $pager->links('group1', 'my_page') ?>
            <?= form_open('reservation/index', ['style' => 'display: flex']); ?>
            <select name="per_page" class="ml-3" aria-label="per_page">
                <option value="2" <?php if ($per_page == '2') echo("selected"); ?>>2</option>
                <option value="5" <?php if ($per_page == '5') echo("selected"); ?>>5</option>
                <option value="10" <?php if ($per_page == '10') echo("selected"); ?>>10</option>
                <option value="20" <?php if ($per_page == '20') echo("selected"); ?>>20</option>
            </select>
            <button class="btn btn-dark" type="submit" class="btn btn-dark">На странице</button>
            </form>
            <?= form_open('reservation/index', ['style' => 'display: flex']); ?>
            <input type="text" class="form-control ml-3" name="search" placeholder="ФИО" aria-label="Search"
                   value="<?= $search; ?>">
            <button class="btn btn-dark" type="submit" class="btn btn-dark">Найти</button>
            </form>
        </div>
        <?php if (!empty($reservation) && is_array($reservation)) : ?>
            <?php foreach ($reservation as $item): ?>
                <div class="card mb-3" style="max-width: 540px; padding: 30px; background: rgba(0,0,0,0.8)">
                    <div class="row">
                        <div class="col-md-4 d-flex align-items-center">
                            <?php if (is_null($item['picture_url'])) : ?>
                                <img src="<?= base_url() ?>/images/avatar.png" class="card-img" style="border-radius: 999px"
                                     alt="<?= esc($item['ID']); ?>">
                            <?php else: ?>
                                <img src="<?= esc($item['picture_url']); ?>" class="card-img" style="border-radius: 999px"
                                     alt="<?= esc($item['ID']); ?>">
                            <?php endif ?>
                        </div>
                        <div class="col-md-8">
                            <div class="card-body" style="color: #fff;">
                                <h5 class="card-title"><?= esc($item['name']); ?></h5>
                                <div class="my-0">Номер комнаты:</div>
                                <p class="card-text"><?= esc($item['ID_room']); ?></p>
                                <a href="<?= base_url() ?>/reservation/view/<?= esc($item['ID']); ?>"
                                   class="btn btn-dark">Просмотреть</a>
                                <p class="card-text"><small class="text-muted"></small></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php else : ?>
            <p>Невозможно найти бронь.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>
<!--Все бронирования array(5)
{
[0]=> array(8) {
["ID"]=> string(1) "3"
["ID_room"]=> string(1) "3"
["ID_guest"]=> string(1) "3"
["date_beginning"]=> string(10) "2021-03-20"
["date_end"]=> string(10) "2021-03-28"
["number_peoples"]=> string(1) "2"
["picture_url"]=> NULL
["name"]=> string(40) "Царь Дмитрий Павлович"
}



[1]=> array(8) { ["ID"]=> string(1) "4" ["ID_room"]=> string(1) "4" ["ID_guest"]=> string(1) "4" ["date_beginning"]=> string(10) "2021-03-18" ["date_end"]=> string(10) "2021-04-01" ["number_peoples"]=> string(1) "1" ["picture_url"]=> NULL ["name"]=> string(48) "Найденкова Мария Игоревна" } [2]=> array(8) { ["ID"]=> string(1) "5" ["ID_room"]=> string(1) "5" ["ID_guest"]=> string(1) "5" ["date_beginning"]=> string(10) "2021-04-01" ["date_end"]=> string(10) "2021-04-10" ["number_peoples"]=> string(1) "4" ["picture_url"]=> NULL ["name"]=> string(44) "Кузьмин Максим Игоревич" } [3]=> array(8) { ["ID"]=> string(1) "2" ["ID_room"]=> string(1) "4" ["ID_guest"]=> string(1) "2" ["date_beginning"]=> string(10) "2021-04-08" ["date_end"]=> string(10) "2021-04-22" ["number_peoples"]=> string(1) "3" ["picture_url"]=> string(55) "https://s3.osipov.digital/surgu/60571gas/file322360.png" ["name"]=> string(56) "Доронин Александра Дмитриевна" } [4]=> array(8) { ["ID"]=> string(1) "2" ["ID_room"]=> string(1) "2" ["ID_guest"]=> string(1) "2" ["date_beginning"]=> string(10) "2021-04-10" ["date_end"]=> string(10) "2021-04-12" ["number_peoples"]=> string(1) "2" ["picture_url"]=> string(55) "https://s3.osipov.digital/surgu/60571gas/file954069.jpg" ["name"]=> string(56) "Доронин Александра Дмитриевна" } }-->

