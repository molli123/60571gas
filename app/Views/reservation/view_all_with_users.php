<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <?php if (!empty($reservation) && is_array($reservation)) : ?>
        <h2>Все бронирования:</h2>
        <table class="table table-striped">
            <thead>
            <th scope="col">Аватар</th>
            <th scope="col">ФИО</th>
            <th scope="col">Дата начала</th>
            <th scope="col">Дата окончаия</th>
            <th scope="col">Номер бронирования</th>
            <th scope="col">Номер комнаты</th>
            <th scope="col">Управление</th>

            </thead>
            <tbody>
            <?php foreach ($reservation as $item): ?>
                <tr>
                    <td>
                        <?php if (is_null($item['picture_url'])) : ?>
                                <img height="150" src="<?= base_url() ?>/family.svg" class="card-img" alt="<?= esc($reservation['ID']); ?>">
                        <?php else:?>
                            <img height="50" src="<?= esc($item['picture_url']); ?>" alt="<?= esc($item['name']); ?>">
                        <?php endif ?>
                    </td>
                    <td><?= esc($item['name']); ?></td>
                    <td><?= esc($item['date_beginning']); ?></td>
                    <td><?= esc($item['date_end']); ?></td>
                    <td><?= esc($item['ID']); ?></td>
                    <td><?= esc($item['ID_room']); ?></td>
                    <td>
                        <a href="<?= base_url()?>/reservation/view/<?= esc($item['id']); ?>" class="btn btn-dark btn-sm">Просмотреть</a>
                        <a href="<?= base_url()?>/reservation/edit/<?= esc($item['id']); ?>" class="btn btn-warning btn-sm">Редактировать</a>
                        <a href="<?= base_url()?>/reservation/delete/<?= esc($item['id']); ?>" class="btn btn-danger btn-sm">Удалить</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

    <?php else : ?>
        <div class="text-center">
            <p>Бронирования не найдены </p>
            <a class="btn btn-dark btn-lg" href="<?= base_url()?>/reservation/create"><span class="fas fa-tachometer-alt" style="color:white"></span>&nbsp;&nbsp;Создать бронирование</a>
        </div>
    <?php endif ?>
</div>
<?= $this->endSection() ?>
